# soal-shift-sisop-modul-1-A09-2022



## soal1:
 
Membuat program agar lebih dari satu user dapat menggunakan fitur unduh gambar dari https://loremflickr.com/320/240

### register.sh

Berfungsi sebagai program yang digunakan oleh para user untuk membuat username beserta passwordnya. Ketentuannya adalah tidak boleh menggunakan username yang sudah ada di file "user.txt" (alias username yang sudah ada) dan untuk password terdapat beberapa ketentuan, yakni:
1. Panjang minimal 8 karakter 
2. Kombinasi huruf kapital, huruf kecil, dan angka (alphanumeric)
3. Tidak boleh sama dengan username

Pembuka.
```
#!/bin/bash
```

Di awal, akan dideklarasikan fungsi "validatePassword" untuk mengecek apakah password sudah sesuai dengan ketentuan yang ada.
```
function validatePassword () {
    local stat=1
    local pass=$1
    LEN=${#pass}
    if [[ $pass =~ [0-9] ]] && [[ $pass =~ [A-Z] ]] && [[ $pass =~ [a-z] ]] && [[ $LEN -ge 8 ]] && [[ $password_register != $username_register ]]
    then stat=$?
    fi
    return $stat
}
```
Kemudian, akan dipersilahkan user untuk menginput username.
```
echo "please enter your username"
read username_register
```

Ketika user menginput username untuk register, akan dicek apakah username yang diinput sudah ada/terdaftar di "user.txt".
```
if grep -rq "username_register" user.txt
    then echo "username already exists"
    date +"%d/%m/%y %T" >> log.txt
    echo "REGISTER:ERROR User $username_register already exists" >> log.txt
    exit 1
else 
    echo "password for $username_register"
fi
```
Jika username ternyata sudah ada, akan diberitahukan kepada user bahwa username itu telah ada lalu "exit 1" akan mengakhiri program "register.sh". Kemudian, akan dicetak tanggal, waktu, dan pesan ketika gagal menginput username ke file "log.txt". Tetapi, jika username yang diinput tidak ditemukan di "user.txt", program akan berlanjut menanyakan password untuk username tadi.

Berikutnya, akan dibaca input password dari user.
```
read -s password_register
```

Setelah itu, fungsi "validatePassword" akan digunakan untuk menguji validitas password yang telah diinput oleh user.
```
validatePassword $password_register
```
Berikutnya, jika password yang diinput user tidak sesuai dengan ketentuan yang ada:
1. Tidak ada username beserta passwordnya yang akan dicetak ke "user.txt";
2. Tidak dicetak tanggal, waktu, dan pesan telah berhasil atau error register ke "log.txt";
3. Program selesai.

Akan tetapi, jika passwordnya sesuai dengan ketentuan yang ada: 
1. Akan dicetak username beserta passwordnya ke "user.txt"; 
2. Dicetak tanggal, waktu, dan pesan telah berhasil register ke "log.txt"; dan
3. Program selesai.
Sebagai tambahan, dicetak pula pesan konfirmasi telah berhasil register ke layar.

```
if [[ $? -ne 0 ]]
    then echo "invalid password!"
else
    echo "$username_register:$password_register" >> user.txt
    date +"%d/%m/%y %T" >> log.txt
    echo "REGISTER:INFO User $username_register registered successfully" >> log.txt
    echo "account created!"
fi
```
Format pencetakan username beserta passwordnya dapat bermacam-macam. Pada kali ini, formatnya adalah 'username:password'. Adanya format pencetakan akan memudahkan proses login di "main.sh" di mana agar seorang user dapat menggunakan fitur download gambar, diperlukan autentikasi dengan username dan password.

### main.sh

Berfungsi sebagai program agar user dapat login dan menggunakan dua fitur, yakni "dl n" untuk mengunduh n gambar dari https://loremflickr.com/320/240 dan "att" untuk mengetahui seberapa banyak login yang telah dilakukan oleh user baik yang berhasil maupun gagal.

Pembuka.
```
#!/bin/bash
```
Di awal, dideklarasikan fungsi "download" dan "att"
```
function download() {
    for ((i=1; i<=n; i=i+1))
        do
            if [ $i -lt 10 ]
                then wget https://loremflickr.com/320/240 -O $folder/PIC_0$i
            else
                wget https://loremflickr.com/320/240 -O $folder/PIC_$i
            fi
            done
            zip -r -e $folder $folder
            rm -r $folder
}

function att() {
    echo "failed login:"
    grep -c "LOGIN:ERROR Failed login attempt on user $username_login" log.txt
    echo "successful login:"
    grep -c "LOGIN:INFO User $username_login" log.txt
}
```
Kemudian, user akan dipersilahkan untuk menginput username yang sudah pernah di-register-kan sebelumnya.
```
echo "please enter your username"
read username_login
```

Berikutnya, akan dicek apakah username tersebut benar-benar ada di "user.txt". Jika username benar-benar ada, user akan dipersilahkan untuk menginput password. Jika tidak, akan dicetak pesan ke layar dan program diselesaikan.
```
if grep -rq "$username_login" user.txt
    then echo "please enter your password"
else
    echo "username $username_login hasn't existed yet"
    exit 1
fi
```

Setelah berhasil login, akan dicetak tanggal, waktu, dan pesan ke "log.txt". Lalu, user dipersilahkan menginput command yang diinginkan. Untuk "dl n", akan digunakan loop untuk mengunduh n gambar lalu menyimpannya dalam suatu folder. Folder tersebut selanjutnya akan dizip dan user akan dimintai password untuk zip tersebut. Nantinya, password yang diinput oleh user akan digunakan ketika zip tersebut ingin dibuka. Untuk "att", akan dicari kalimat "LOGIN:INFO ..." dan menghitung jumlah kalimatnya, begitu pula untuk "LOGIN:ERROR ...". Sehingga, akan didapatkan jumlah login yang berhasil dan gagal dari seorang user.
```
read -s password_login

if grep -Fxq "$username_login:$password_login" log.txt
    then date +"%d/%m/%y %T" >> log.txt
    echo "LOGIN:INFO User $username_login logged in" >> log.txt

    date=$(date +"%Y-%m-%d")
    folder="${date}_$username_login"
    mkdir "$folder"

    echo "please enter your command (dl n / att)"
    read command n

    if [[ $command = "dl" ]]
        then download
    elif [[ $command = "att" ]]
        then att
    else
        echo "wrong command!"
        exit 1
    fi

else 
    echo "incorrect password!"
    date +"%d/%m/%y %T" >> log.txt
    echo "LOGIN:ERROR Failed login attempt on user $username_login" >> log.txt

fi
```

## soal2:
### soal2_forensic_dapos.sh

2.A Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.
```
mkdir forensic_log_website_daffainfo_log
```
2.B Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
```
function rata_rata() {
i=0
n=12
sum=0

while [ $i -le $n ]
do
 if [ $i -lt 10 ]
 then count=$(grep -c "Jan/2022:0$i" log_website_daffainfo.log)
  sum=$((sum+count))
 else count=$(grep -c "Jan/2022:$i" log_website_daffainfo.log)
  sum=$((sum+count))
 fi
 ((i++))
done
printf 'Rata-rata serangan adalah sebanyak %.3f requests per jam'  $(echo "$sum/12" | bc -l) >> $folder/ratarata.txt
}

```
Masukkkan ratarata ke dalam file bernama ratarata.txt yang telah dibuat sebelumnya
```
>> /home/hapis
forensic_log_website_daffainfo_log/ratarata.txt
```
2.C Dalam soal ini penyerangan ke website https://daffa.info menggunakan banyak sekali IP sehingga dimnta untuk menampilkan IP yang paling banyak melakukan request ke server dan juga menampilkan berapa banyak request yang dikirimkan dengan IP tersebut. dengan memasukkan output kedalam file baru bernama result.txt yang telah dibuat sebelumnya.
```
function paling_banyak {
cat $log | awk -F: '{gsub(/"/, "", $1) arr[$1]++}
 END {
  max=0
  target
  for (i in arr){
   if (max < arr[i]){
    target = i
    max = arr[target]
   }
  }
```
lalu diprint IP yang paling banyak mengakses server sebanyak request yang diminta
```
 print "IP yang paling banyak mengakses server adalah: " target " sebanyak " max " requests\n"
 }' >> $folder/result.txt
```
2.D Dalam penyerangan itu juga banyak request ada yang menggunakan user-agent ada juga yang tidak yang menggunakan user-agent. Dan dalam beberapa request, banyak request yang menggunakan user-agent. Diperlukan fungsi curl yang berfungsi untuk mencari kata-kata yang terdapat kata curl
```
cat $log | awk '/curl/ {n++} END {
```
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
```
print "Ada " n " requests yang menggunakan curl sebagai user-agent\n"}' >> $folder/result.txt
}
```
2.E Pada jam 2 pagi di tanggal 23 terdapat penyerangan pada website https://daffa.info. oleh karena itu diminta untuk mencari tahu daftar IP yang mengakses website pada jam tersebut. Menggunakan fungsi jam_2 untuk mendapatkan daftar IP yang mengakses pada jam tersebut
```
function jam_2() {
cat $log | awk -F: '/2022:02/ {gsub(/"/, "", $1) arr[$1]++}
 END {
  for (i in arr){
   print i " jam 2 pagi"
  }
```
Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.
```
 }' >> $folder/result.tx
```

## soal3:
Membuat program untuk monitoring memory dan disk usage sebuah komputer.
### minute_log.sh
Program ini ditujukan untuk menghitung memory dan disk usage setiap menitnya
```
#!/bin/bash

date=$(date +"%Y%m%d%H%M") > transit.txt
free >> transit.txt
du -sh /home/hapis >> transit.txt
```
Pada potongan script di atas, pertama akan didapatkan waktu saat script dijalankan yang kemudian akan digunakan sebagai nama file log tersebut. Lalu, "free" digunakan untuk mendapatkan informasi mengenai memory dan "du -sh" akan menampilkan informasi mengenai disk usage suatu path. Ketiganya tadi akan secara sementara ditaruh di sebuah file bernama "transit.txt".
```
awk '/Mem/ {printf "%s,%s,%s,%s,%s,%s,",$2,$3,$4,$5,$6,$7} /Swap/ {printf "%s,%s,%s,",$2,$3,$4} /home/ {printf "%s,%s",$2,$1}' transit.txt >> ./log/metrics_${date}00.log
```
Kemudian, menggunakan "awk", akan dicetak informasi mengenai memory dan disk usage ke sebuah file log yang akan dinamai sesuai dengan waktu monitoring terjadi.
```
chmod u=rw ./log/metrics_${date}00.log
chmod og=--- ./log/metrics_${date}00.log
```
Terakhir, agar file-file log tadi hanya bisa dimanipulasi oleh user, maka kita akan gunakan "chmod" untuk mengaturnya. "chmod u=rw" akan mengatur permission user menjadi dapat read dan write. "chmod og=---" akan mengatur permission group dan others menjadi tidak ada sama sekali (read, write, maupun execute).
### aggregate_minutes_to_hourly_log.sh
Program ini ditujukan untuk menampilkan informasi mengenai tiap-tiap indikator dalam monitoring memory dan disk usage berupa maximum, minimum, dan average-nya.
