#!/bin/bash

function download() {
    for ((i=1; i<=n; i=i+1))
        do
            if [ $i -lt 10 ]
                then wget https://loremflickr.com/320/240 -O $folder/PIC_0$i
            else
                wget https://loremflickr.com/320/240 -O $folder/PIC_$i
            fi
            done
            zip -r -e $folder $folder
            rm -r $folder
}

function att() {
    echo "failed login:"
    grep -c "LOGIN:ERROR Failed login attempt on user $username_login" log.txt
    echo "successful login:"
    grep -c "LOGIN:INFO User $username_login" log.txt
}

echo "please enter your username"
read username_login

if grep -rq "$username_login" user.txt
    then echo "please enter your password"
else
    echo "username $username_login hasn't existed yet"
    exit 1
fi

read -s password_login

if grep -Fxq "$username_login:$password_login" log.txt
    then date +"%d/%m/%y %T" >> log.txt
    echo "LOGIN:INFO User $username_login logged in" >> log.txt

    date=$(date +"%Y-%m-%d")
    folder="${date}_$username_login"
    mkdir "$folder"

    echo "please enter your command (dl n / att)"
    read command n

    if [[ $command = "dl" ]]
        then download
    elif [[ $command = "att" ]]
        then att
    else
        echo "wrong command!"
        exit 1
    fi

else 
    echo "incorrect password!"
    date +"%d/%m/%y %T" >> log.txt
    echo "LOGIN:ERROR Failed login attempt on user $username_login" >> log.txt

fi

