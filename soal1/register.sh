#!/bin/bash

function validatePassword () {
    local stat=1
    local pass=$1
    LEN=${#pass}
    if [[ $pass =~ [0-9] ]] && [[ $pass =~ [A-Z] ]] && [[ $pass =~ [a-z] ]] && [[ $LEN -ge 8 ]] && [[ $password_register != $username_register ]]
    then stat=$?
    fi
    return $stat
}

echo "please enter your username"
read username_register

if grep -rq "username_register" user.txt
    then echo "username already exists"
    date +"%d/%m/%y %T" >> log.txt
    echo "REGISTER:ERROR User $username_register already exists" >> log.txt
    exit 1
else 
    echo "password for $username_register"
fi

read -s password_register

validatePassword $password_register

if [[ $? -ne 0 ]]
    then echo "invalid password!"
else
    echo "$username_register:$password_register" >> user.txt
    date +"%d/%m/%y %T" >> log.txt
    echo "REGISTER:INFO User $username_register registered successfully" >> log.txt
    echo "account created!"
fi
