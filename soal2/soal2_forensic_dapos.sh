#!/bin/bash

dir=/home/hapis
log=$dir/log_website_daffainfo.log
folder=$dir/forensic_log_website_daffainfo_log

function folder() {
if [[ -d "$folder" ]]
then rm -rf $folder
 mkdir $folder
else
 mkdir $folder
fi
}

function rata_rata() {
i=0
n=12
sum=0

while [ $i -le $n ]
do
 if [ $i -lt 10 ]
 then count=$(grep -c "Jan/2022:0$i" log_website_daffainfo.log)
  sum=$((sum+count))
 else count=$(grep -c "Jan/2022:$i" log_website_daffainfo.log)
  sum=$((sum+count))
 fi
 ((i++))
done
printf 'Rata-rata serangan adalah sebanyak %.3f requests per jam'  $(echo "$sum/12" | bc -l) >> $folder/ratarata.txt
}

function paling_banyak {
cat $log | awk -F: '{gsub(/"/, "", $1) arr[$1]++}
 END {
  max=0
  target
  for (i in arr){
   if (max < arr[i]){
    target = i
    max = arr[target]
   }
  }
  print "IP yang paling banyak mengakses server adalah: " target " sebanyak " max " requests\n"
 }' >> $folder/result.txt
}

function curl() {
cat $log | awk '/curl/ {n++} END {
print "Ada " n " requests yang menggunakan curl sebagai user-agent\n"}' >> $folder/result.txt
}

function jam_2() {
cat $log | awk -F: '/2022:02/ {gsub(/"/, "", $1) arr[$1]++}
 END {
  for (i in arr){
   print i " jam 2 pagi"
  }
 }' >> $folder/result.txt
}

folder
rata_rata
paling_banyak
curl
jam_2
