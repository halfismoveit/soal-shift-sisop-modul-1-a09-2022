#!/bin/bash

# unfinished

date=$(date +"%Y%m%d%H")
ls ./log > transitloglist.txt
count=$(grep -c "${date}" transitloglist.txt)

i=$((60-count))
k=00

while [ $i -lt 60 ]
	do
		if [ $i -lt 10 ]
		then awk '{gsub(/M/,""); print}' ./logtemp/metrics_20220305180$i$k.log >> transitm.txt
		else awk '{gsub(/M/,""); print}' ./logtemp/metrics_2022030518$i$k.log >> transitm.txt
		fi

	awk 'BEGIN{FS=","}{
		printf("mem_total=%s\n",$1)
		printf("mem_used=%s\n",$2)
		printf("mem_shared=%s\n",$4)
		printf("mem_buff=%s\n",$5)
		printf("mem_available=%s\n",$6)
		printf("swap_total=%s\n",$7)
		printf("swap_used=%s\n",$8)
		printf("swap_free=%s\n",$9)
		printf("path_size=%s\n",$11)
	}
	' transitm.txt >> transitlogvalue.txt

	source transitlogvalue.txt
		sum_mem_total=$((sum_mem_total+mem_total))
		sum_mem_used=$((sum_mem_used+mem_used))
		sum_mem_free=$((sum_mem_free+mem_free))
		sum_mem_shared=$((sum_mem_shared+mem_shared))
		sum_mem_buff=$((sum_mem_buff+mem_buff))
		sum_mem_available=$((sum_mem_available+mem_available))
		sum_swap_total=$((sum_swap_total+swap_total))
		sum_swap_used=$((sum_swap_used+swap_used))
		sum_swap_free=$((sum_swap_free+swap_free))
		sum_path_size=$((sum_path_size+path_size))

	((i++))
	done

echo "average,$((sum_mem_total/count)),$((sum_mem_used/count)),$((sum_mem_used/count)),$((sum_mem_free/count)),$((sum_mem_shared/count)),$((sum_mem_buff/count)),$((sum_mem_available/count)),$((sum_swap_total/count)),$((sum_swap_used/count)),$((sum_swap_free/count)),/home/hapis,$((sum_path_size/count))" >> metrics_agg_${date}.log

rm transitm.txt
rm transitlogvalue.txt
