#!/bin/bash

date=$(date +"%Y%m%d%H%M") > transit.txt
free >> transit.txt
du -sh /home/hapis >> transit.txt

awk '/Mem/ {printf "%s,%s,%s,%s,%s,%s,",$2,$3,$4,$5,$6,$7} /Swap/ {printf "%s,%s,%s,",$2,$3,$4} /home/ {printf "%s,%s",$2,$1}' transit.txt >> ./log/metrics_${date}00.log
chmod u=rw ./log/metrics_${date}00.log
chmod og=--- ./log/metrics_${date}00.log
